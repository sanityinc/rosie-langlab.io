---
layout: default
title: Documentation
permalink: /doc/
---

* Documentation for Rosie v1.0.0 is at 
[https://gitlab.com/rosie-pattern-language/rosie#contents](https://gitlab.com/rosie-pattern-language/rosie#contents).

* [Rosie man page](rosie.1.html)




