---
layout: default
title: Examples
permalink: /ex/
---

Examples of Rosie v0.99, as well as some blog posts and Tech Talks, may be found on 
[IBM developerWorks Open](https://developer.ibm.com/code/search/?q=rosie).

There were some changes when Rosie v1.0.0 was released, so please be aware the
the IBM content may not match exactly what Rosie looks like today.

This page, here on `rosie-lang.org`, will eventually contain a collection of
examples for Rosie v1.0.0.


